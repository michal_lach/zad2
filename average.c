float average(float *tab, int x, int k)  //funkcja dla sredniej
{
    int i = 0;
    float srednia = 0.0;
    for (i = 0; i < x; i++)
    {
        srednia = srednia + tab[i];
    }
    srednia = srednia / i;

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Srednia dla calej kolumny %s wynosi: %f\n", p, srednia);  //informacja o średniej
    return srednia;
}