/* Napisać program, który zrobi to samo co program pierwszy, ale zapisze zaczytane
dane oraz wyniki do struktury. Program skorzysta z modułów do statystyk opisowych.
Program sprawdzi, czy w pliku są już dopisane statystyki i jeśli ich nie ma, dopisze je.
W modułach mają znaleźć się tylko funkcje/procedury dla statystyk, ewentualnie definicja struktury.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "fun.h"



struct Statystyki   //strutktura dla statystyk
{
    float average;
    float median;
    float deviation;
};

int main()
{
    FILE *fp;   //kod wczytujący tablice z pliku do trzech dynamicznych tablic floatów
    int x = 50;
    if ((fp = fopen("plik.txt", "r")) == NULL)
    {
        printf("Error");
        exit(1);
    }
    char p[10] = "";
    char q[10] = "";
    char r[10] = "";
    char s[10] = "";
    fscanf(fp, "%s\t%s\t\t%s\t\t%s\n", p, q, r, s); //zczytanie nagłówka
    float *tab0;
    float *tab1;
    float *tab2;
    int i = 0;
    tab0 = (float*)malloc(x*sizeof(float));    //zaalokowanie tablic dynamicznych
    tab1 = (float*)malloc(x*sizeof(float));
    tab2 = (float*)malloc(x*sizeof(float));
    float t = 0.0;
    float u = 0.0;
    float v = 0.0;
    for (i = 0; i < x; i++) //pętla czytająca każdy wiersz
    {
        fscanf(fp, "%s\t%f\t\t%f\t%f\n", s, &t, &u, &v);
        tab0[i] = t;
        tab1[i] = u;
        tab2[i] = v;
    }
    
    int k = 0;
    struct Statystyki A;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    A.average = average(tab0, x, k);    //funkcja dla sredniej kolumny
    A.median = mediantab(tab0, x, k); //funkcja dla mediany
    A.deviation = odchylenietab(tab0, x, k, A.average);    //funkcja dla odchylenia

    k = 1;
    struct Statystyki B;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    B.average = average(tab1, x, k);    //funkcja dla sredniej kolumny
    B.median = mediantab(tab1, x, k); //funkcja dla mediany
    B.deviation = odchylenietab(tab1, x, k, B.average);    //funkcja dla odchylenia

    k = 2;
    struct Statystyki C;
    //druktab(tab0, x);   //funkcja dla pokazania tablicy
    C.average = average(tab2, x, k);    //funkcja dla sredniej kolumny
    C.median = mediantab(tab2, x, k); //funkcja dla mediany
    C.deviation = odchylenietab(tab2, x, k, C.average);    //funkcja dla odchylenia

    dopisz("plik.txt", A.average, A.median, A.deviation, B.average, B.median, B.deviation, C.average, C.median, C.deviation);

    return 0;
}








