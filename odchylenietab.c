#include "fun.h"
float odchylenietab(float *tab, int x, int k, float srednia)   //funkcja dla odchylenia
{
    int i = 0;
    float temp = 0.0;
    for (i = 0; i < x; i++)
    {
        temp = temp + pow(tab[i] - srednia, 2);
    }
    temp = temp / x;
    temp = sqrt(temp);

    char p[4] = "";
    if (k == 0)
    {
        strcpy(p, "X");
    }
    else if (k == 1)
    {
        strcpy(p, "Y");
    }
    else if (k == 2)
    {
        strcpy(p, "RHO");
    }

    printf("Odchylenie standardowe kolumny %s wynosi: %f\n\n", p, temp);
    return temp;
}
